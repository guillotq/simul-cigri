with import <nixpkgs> {};
mkShell {
	name = "shell";
	buildInputs = [
      jupyter
      python38Packages.numpy
      python38Packages.matplotlib
	];
	shellHook = ''
      jupyter-notebook cigri_simul.ipynb
	'';
}
