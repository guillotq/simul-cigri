from setuptools import setup

setup(
    # Application name:
    name="SimulCiGri",

    # Version number (initial):
    version="0.1.0",

    # Application author details:
    author="Quentin Guilloteau",
    author_email="Quentin.Guilloteau@grenoble-inp.org",

    # Packages
    packages=["app"],

    # Include additional files into the package
    # include_package_data=True,
    entry_points={
        'console_scripts': ['simul_cigri=app.simul_cigri:main'],
    },

    # Details
    url="https://github.com/GuilloteauQ/simul_cigri",

    #
    # license="LICENSE.txt",
    description="Simulator for Cigri",

    # long_description=open("README.txt").read(),

    # Dependent packages (distributions)
    install_requires=[
    ]
)
