---
title: Running CiGri Experiments on Grid'5000
author: Quentin Guilloteau
date: 03/12/2020
---

(Reminder: you can copy-paste in grid5000 by using `Ctrl-Maj-v` instead of `Ctrl-v`)

# Connecting to the Nancy Frontend

In order to connect to the Nancy frontend, we must first connect to the access point of grid5000.

If you are on Windows, use PuTTY, on Linux use the `ssh` command.

Connect at the following host:

```
<YOUR_LOGING>@access.grid5000.fr
```

Once you are connected, run the following command:

```sh
ssh nancy
```

You should now be on the Nancy frontend !

To check if you are indeed on it, you can:

- look at the beginning of the line of your terminal, is should be `<YOUR_LOGING>@fnancy`

- execute the command `hostname`, it should return `fnancy`


# Running an Experiment

We are now on the Nancy site.

If the installation went well, there should be a file name `run_experiments.org` in the current directory (your `HOME` directory).
If this is not the case, use the following file: `~/NIX/cigri/experiments/run_experiments.org`

## Open the Notebook

To open the file, execute the following command:

```sh
emacs run_experiments.org
```

This should open the `emacs` program with our `org` file.

## Things to know about `emacs`

Emacs is a powerful text editor, with special features that allow us to execute command from a file.

This is what we are going to use to run the experiments.

To move in `emacs` use the arrows keys.

To quit `emacs` use the following: `Ctrl-x Ctrl-c`

It might ask you if you really want to quit, or save in the bar below, type `yes` or `no` (depending on what you want to do) (most of the time I don't think you should save the `run_experiments.org` file).

## Things to know about Org-Mode

Org-Mode is the powerful `emacs` feature (mode) that allows us to run code blocks and more.

You will see that when freshly open, the `run_experiments.org` file look small ! But it is actually folded !

Indeed, org mode provide a hierarchy folding mechanism for the structure of the document.

Lines starting with an `*` are the main sections. Lines starting with `**` are the subsections, `***` subsubsections etc.

To fold and unfold sections, simply put the cursor on the line and press `Tab`

## How to execute a Code Block

A code block in `emacs` is delimited by the tags `#+BEGIN_SRC` and `#+END_SRC`.

In order to execute a code block, you need to put the cursor on a code block and press: `Ctrl-c Ctrl-c`.

`emacs` will most likely ask you if you want to execute the code block on your system. Say `yes`.

The code block has now been executed.

## Structure of the `run_experiments.org` Document

The `run_experiments.org` document has one section (for the moment): `Grid5000`.
This section has 2 subsections: `Setup` and `Experiment Template`.

There is no need to execute the codeblocks of the `Setup` section, the installation script has done it.

In the `Experiment Template` section, you will see some subsubsections.
Each of them repesent a step of the exprimental pipeline.
Some steps are not mandatory.
When a step is mandatory, there will be written `:Mandatory:` on the right of the section's name.

## Which Codeblock can you modify ?

Not all of them.
You can change the values of in the JSON config of the controllers and the definitions of the campaigns.

# An Experiment Pipeline

## Pick the deployment Setup

Chose the setup that you want.
If it is not available in the notebook, send me an email

## Pick the config for CiGri

I recommend you use the default configuration (execute the codeblock of the `Use default config` section).
However, if you want to change the sampling time of CiGri for instance, you would need to change the values in the custom config (send me an email if unsure).

## Pick the controller to use

Execute the codeblock corresponding at the controller you want to use

## Pick the setup for the Controller

Dont forget to execute the init codeblock !

Then, open the section of the controller you chose previously.
There are two subsections.
The first one contains the JSON file corresponding to the setup of the controllers (e.g. the matices values of the MPC).
You can change the values as you wish.
No need to execute the JSON code block (it start by `#+BEGIN_EXAMPLE` not `#+BEGIN_SRC`).
Then save the config by executing the code block in the section of the same name.

## Campaigns to submit

Don't forget the initial codeblock !

You can then modify the description of the campaign.
Note that you cannot use a file size > 0 if you did not pick a deployment setup with a fileserver.

You can change the values of `NB_JOBS`, `SLEEP_TIME` and `FILE_SIZE` (don't worry about `HEAVINESS`).
Once you changed the values, execute the codeblock to save the campaign.

You can execute this codeblock as many times as you want if you want to run several campaigns. during the same experiment.

## Submit the experiment

(Even if i forgot to put the Mandatory tag on this section, it is mandatory).

Execute the first code block, it will generate the command to execute to start the experiment, but it will not execute it yet.

The two code blocks following are just for debug purposes, don't bother.

The last code block (`eval ${base_command} &`) is the one to run to start the experiment.

## Now what ?

At this point you can actually close `emacs`, it will probably be easier.
Don't bother with the following sections of the document.

### Check for progess

Once you are out of `emacs`, run the following command:

```sh
oarstat -u
```

If you see a line, it indicates that you have a experiment running !

The first 15 minutes after starting the experiments is the deployment of the correct system, which takes time.

At some point you will be able to run the following command without an error message:

```sh
cat gridstat.output
```

It represents the progress of the campaign(s) submitted.

### When is the expriment over ?

The experiment should hand over the resources taken to Grid5000 at the end of the experiment.

If the command:

```sh
oarstat -u
```

returns nothing, then the experiment is over.

### Where is the data resulting of the experiment ?

Go to your favorite web browser and enter the following URL:

```
https://api.grid5000.fr/sid/sites/nancy/public/<YOUR_LOGIN>/
```

(don't forget the last `/`)

This will ask you your Grid5000 username and password.

You can then see all your experiences.
Each experiment is stored in a folder with the date of the experiment on it.
In each folder, there are two files:

- a `notebook` which gathers all the informations required to do the same experiment again

- a `log` file in the CSV format. Each controller has a specific format for the log file, the simplest is that you send me an email to know the format (or you might also dare to look in the code, in the ~/NIX/cigri/lib/cigri-control.rb file)

If you click on the `log` file, your browser should download it, you can then do the data analysis on your own machine.

# What are the possible errors ?

Our system is really complex and errors can come from anywhere, anytime...

The most common errors are the following:

## OAR blocks in Finishing state

When executing `cat gridstat.ouput`, you might see that there is no progress since the last time you called it.
(Make sure it has been at least the execution time of the jobs of the campaigns ...).

In this case, it is probably OAR which is stuck in Finishing state (i.e. it does not manage to kill the terminated jobs).
This issue is known to the OAR team, but there is no fix yet.

The solution: give up.
On the frontend run the following command:

```sh
oarstat -u -J | jq -r "to_entries[0].key" | xargs -I {} oardel {}
```

It will stop the experiment.

Wait until the output of `oarstat -u` is empty, and start a new experiment.

## The deployement fails

This is the one I am the most afraid of because the notebook approach hides this issue from us a bit.
In theory if you make sure that the output of `oarstat -u` is empty, you should be out of trouble.

## CiGri error

It might happen that CiGri crashes, it happens.
You can detect it the same way you detect if OAR is stuck, by noticing no progress for a long time.
Moreover, if you see that the state of the campaigns is not `R` but `Re` then you are sure this is a CiGri error ...

First execute this command:

```sh
CIGRI_SERVER=$(oarstat -u -J | jq -r 'to_entries[].value.assigned_network_address[0]')
scp ${CIGRI_SERVER}:/tmp/cigri.log ~/public/cigri_$(date +%s).log
```

Then send me a mail telling you had a cigri crash, and I will look at it.


Then follow the same procedure as when there is a OAR error.
