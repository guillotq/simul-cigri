

class OAR:
    def __init__(self, resources):
        self.max_resources = resources
        self.fileserver_load = 0
        self.current_time = 0
        self.running_jobs = []
        self.waiting_jobs = []
        self.dict_has_been_executed = {}

    def get_nb_free_resources(self):
        return self.max_resources - len(self.running_jobs)

    def get_time_next_finishing_job(self):
        if len(self.running_jobs) == 0:
            return 5
        return min(map(lambda x: x.duration, self.running_jobs))

    def remove_finished_jobs(self):
        # Get the duration to the next finishing job
        min_duration_time = self.get_time_next_finishing_job()
        updated_running_jobs = []
        for j in self.running_jobs:
            if j.duration <= min_duration_time:
                self.dict_has_been_executed[j.job_id] = True
            else:
                j.duration = j.duration - min_duration_time
                updated_running_jobs.append(j)
        # Keep only the jobs that are not done after the moment the first jobs finishes
        self.running_jobs = updated_running_jobs
        # Update the current time of OAR
        self.current_time = self.current_time + min_duration_time
        # TODO: fileserver load

    def has_been_executed(self, job_id):
        return self.dict_has_been_executed[job_id]

    def submit_job(self, job):
        self.waiting_jobs.append(job)
        self.dict_has_been_executed[job.job_id] = False

    def schedule_jobs(self):
        nb_free_resources = self.get_nb_free_resources()
        i = 0
        initial_number_of_waiting_jobs = len(self.waiting_jobs)
        while i < initial_number_of_waiting_jobs and i < nb_free_resources:
            self.running_jobs.append(self.waiting_jobs.pop())
            i = i + 1

    def log(self, current_time):
        print("{}, {}, {}".format(current_time, len(self.waiting_jobs), len(self.running_jobs)))


class Job:
    job_id = 0
    def __init__(self, duration, has_io_load):
        self.duration = duration
        self.has_io_load = has_io_load
        self.job_id = Job.job_id
        Job.job_id = Job.job_id + 1

class Campaign:
    campaign_id = 0
    def __init__(self, nb_jobs, duration, has_io_load):
        self.jobs = [Job(duration, has_io_load) for _ in range(nb_jobs)]
        self.nb_jobs = nb_jobs
        self.nb_remaining_jobs_to_execute = nb_jobs
        self.campaign_id = Campaign.campaign_id
        Campaign.campaign_id = Campaign.campaign_id + 1

class CiGri:
    def __init__(self, oar_server, sleep_time = 5):
        self.sleep_time = sleep_time
        self.campaigns = []
        self.oar_server = oar_server
        self.job_ids_of_latest_submission = []

    def submit_campaign_to_cigri(self, campaign):
        self.campaigns.append(campaign)

    def get_jobs_to_submit_to_oar(self, campaign_id, n):
        jobs_to_submit = []
        i = 0
        while i < n and i < self.campaigns[campaign_id].nb_remaining_jobs_to_execute:
            jobs_to_submit.append(self.campaigns[campaign_id].jobs.pop())
            i = i + 1
        return jobs_to_submit

    def submit_jobs_to_oar(self, jobs_to_submit):
        for j in jobs_to_submit:
            self.job_ids_of_latest_submission.append(j.job_id)
            self.oar_server.submit_job(j)

    def is_time_for_new_submission(self):
        for job_id in self.job_ids_of_latest_submission:
            if not self.oar_server.has_been_executed(job_id):
                return False
        return True

    def update_data_camaign(self, campaign_id):
        self.campaigns[campaign_id].nb_remaining_jobs_to_execute -= len(self.job_ids_of_latest_submission)
        self.job_ids_of_latest_submission = []

    def get_sum_nb_jobs_remaining(self):
        return sum(map(lambda c : c.nb_remaining_jobs_to_execute, iter(self.campaigns)))


    def run(self):
        n = 100
        current_time = 0
        next_event_cigri = self.sleep_time
        next_event_oar = next_event_cigri

        while self.get_sum_nb_jobs_remaining() > 0:
            if next_event_cigri <= next_event_oar:
                self.oar_server.log(current_time)
                if self.is_time_for_new_submission():
                    for i in range(len(self.campaigns)):
                        self.update_data_camaign(i)
                    # TODO: not i, but lowest id of remaining camapaigns
                    try:
                        min_remaining_campaign_id = min(map(lambda c: c.campaign_id, filter(lambda c: c.nb_remaining_jobs_to_execute > 0 ,self.campaigns)))
                    except:
                        # print("no more campaigns")
                        return
                    jobs = self.get_jobs_to_submit_to_oar(min_remaining_campaign_id, n)
                    self.submit_jobs_to_oar(jobs)
                current_time += next_event_cigri
                next_event_oar = next_event_oar - next_event_cigri
                next_event_cigri = self.sleep_time
            else:
                self.oar_server.remove_finished_jobs()
                self.oar_server.schedule_jobs()
                current_time += next_event_oar
                next_event_cigri = next_event_cigri - next_event_oar
                next_event_oar = self.oar_server.get_time_next_finishing_job()


def main():
    nb_resources = 16
    oar_server = OAR(nb_resources)
    cigri = CiGri(oar_server)
    c1 = Campaign(1000, 33, True)
    cigri.submit_campaign_to_cigri(c1)

    cigri.run()
    return 0

if __name__ == "__main__":
    main()
