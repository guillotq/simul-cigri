with (import <nixpkgs>{});

let
  app_name = "simul-cigri";
  app_version = "1.0";
in {
  ctrl_email = python37Packages.buildPythonPackage rec {
    name = "${app_name}";
    version = "${app_version}";

    src = ./.;
    propagatedBuildInputs = with python37Packages; [
    ];

    doCheck = false;

    postInstall = ''
      cp -r app/ $out
    '';
  };
}
